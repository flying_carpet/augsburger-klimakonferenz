#!/usr/bin/env bash

perl -i -pwe '
  my $preload = '\''
    <link rel="preload" href="/images/konferenz-symbolbild-cropped-blur.webp" as="image" type="image/webp">
  '\'';

  s/(<link rel=stylesheet.*?>)/$preload$1/ or
  s/<style>/$preload<style>/m;
' public/index.html

find public -name '*.html' -print0 | xargs -0 perl -i -pwe '
  my $base = "https://www.augsburger-klimakonferenz.de/";

  sub fixup {
    my $path = shift;
    $path = substr($base,0,-1) . $path if $path =~ /^\//;
    return $path unless $path =~ /^\Q$base/;

    $path =~ s/^\Q$base//;

    my $level = () = $ARGV =~ /\//g;

    return ("../" x ($level - 1)) . $path;
  }

  s/href="(.*?)"/"href=\"" . fixup($1) . "\""/eg;
  s/src="(.*?)"/"src=\"" . fixup($1) . "\""/eg;
  s/url\(("?)(.*?)("?)\)/"url($1" . fixup($2) . "$3)"/eg;
  s/url\('\''(.*?)'\''\)/"url('\''" . fixup($1) . "'\'')"/eg;
'
