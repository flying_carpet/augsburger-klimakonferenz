+++
title = 'Mitmachen'
featured_image = '/images/konferenz-symbolbild-cropped-blur.webp'
+++

## Teilnahme als Interessierte\*r

Wenn du alleine, mit Freund\*innen oder Familie an der Konferenz teilnehmen willst, musst du dich nicht anmelden. Komm einfach vorbei!
Der Eintritt ist frei!

## Teilnahme als Initiative

Wenn du mit deiner Initiative auf der Konferenz präsent sein und dabei einen Vortrag halten, einen Stand aufstellen oder Ähnliches willst, bist du ebenfalls herzlich willkommen!
Kontaktiere für weitere Planung bitte die Orga der Klimakonferenz unter:
[+4917695110311](tel:+4917695110311) (SMS, Anruf) oder klimakonferenz@systemli.org

## Teilnahme als Vortragende

Du möchtest auf der Konferenz einen Vortrag halten? Dann stell uns deine Idee und den groben zeitlichen Rahmen kurz vor!
Schreib uns gerne.
Wir freuen uns, von dir zu hören :)
