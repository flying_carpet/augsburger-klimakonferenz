+++
title = 'Anfahrt'
description = 'zur Konferenz'
featured_image='/images/konferenz-symbolbild-cropped-blur.webp'
+++

## Adresse

Zeughaus  
Zeugplatz 4  
86150 Augsburg

[Stadtplan](https://geoportal.augsburg.de/WebDaten/externalcall.jsp?client=auto&project=stadtplan&query=lageStadtplan_p&keyname=ID&keyvalue=1081)

## Anreise per ÖPNV

**Tram 1 oder 2 bis Moritzplatz**, 1 Minute Fußweg

## Anreise per Rad

**Fahrradstellplätze direkt am Zeughaus** oder alternativ am Königsplatz stehen zur Verfügung
