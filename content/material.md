+++
title = 'Material'
featured_image='/images/konferenz-symbolbild-cropped-blur.webp'
+++

### Material

#### Eröffnung

* [Steigende CO₂-Emissionen nach internationalen Klimakonferenzen und -abkommen](https://desdemonadespair.net/2022/01/graph-of-the-day-atmospheric-co2-vs-global-temperature-change-and-climate-conference-dates-1958-2020.html)
* [Nur drei Flächenstaaten lagerten insgesamt mehr CO₂ in der Atmosphäre ab](https://klimagerechtigkeit.madeingermany.lol/jx85qK1ztAc.mp4) (Stand 2018)

#### Birgit Zech (Psychologists for Future e.V.): Auswirkungen der Klimakrise auf unsere Gesundheit

* [Folien](/files/zech-gesundheit.pdf)
