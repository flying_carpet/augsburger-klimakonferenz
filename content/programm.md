+++
title = 'Programm'
featured_image='/images/konferenz-symbolbild-cropped-blur.webp'
+++

### Programm

- *14:30* **Get Together**

- *15:00* **Eröffnung**

- *15:15–16:45* **Erste Vortragssession**
	- *Prof. Dr. Angela Oels*: **Bericht von der COP28 in Dubai**
	- *Pax Christi*: **Ausstellung Friedensklima: Klimagerechtigkeit aus christlicher Sicht**
	- *Parents for Future*: **Die Deutsche Bahn - Zustandsbericht eines Insiders und Rolle in der Mobilitätswende**

- *16:45–17:00* **Pause und Gelegenheit, die Stände im Foyer zu besuchen**

- *17:00–18:10* **Workshops mit Kurzvorträgen und Diskussion**
	- Mobilitätswende
	- Energiewende
	- Stadtplanung und Klimaresilienz
	- Gesundheit

- *18:10–18:20* Impuls von *Johannes Enzler*: **Biodiversitäts- und Klimakrise**

- *18:20–19:00* **Ergebnisvorstellung und Podiumsdiskussion mit Akteur\*innen der Lokalpolitik**, mit:
	- Stefan Wagner (Grüne)
	- Wolfgang Keller (ÖDP)
	- SPD (keine Rückmeldung)
	- CSU (Absage)

- *19:00* **Konferenzende und Ausklang** (Restaurant, alternativer Weihnachtsmarkt)
