+++
title = '1. Augsburger Mitmach-Klimakonferenz'
description = 'am 10.12.2023'
featured_image='/images/konferenz-symbolbild-cropped-blur.webp'
draft = false
+++

Die **erste Augsburger Mitmach-Klimakonferenz** bietet allen Interessierten die Gelegenheit, sich über **lokale und allgemeine Aspekte der Klimakrise** und **Lösungsmöglichkeiten** zu informieren.

Doch das ist nicht alles!

Im Gegensatz zur Weltklimakonferenz dürfen alle Teilnehmenden selbst partizipieren und politische, wirtschaftliche oder gesellschaftliche Konzepte und Lösungen aktiv mitgestalten und sind dazu herzlich eingeladen.

Die Konferenz findet am **10.12.2023** von **15 bis 19 Uhr** im [**Zeughaus (Reichlesaal)**](/anfahrt) statt. Das gesamte [Programm](/programm) findest Du [hier](/programm).
